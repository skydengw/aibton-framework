/**
 * Aibton.com Inc.
 * Copyright (c) 2004-2017 All Rights Reserved.
 */
package com.aibton.framework.enums;

import com.aibton.framework.config.AibtonConstantKey;
import com.aibton.framework.enums.inter.IEnum;

/**
 * @author huzhihui
 * @version v 0.1 2017/5/11 22:22 huzhihui Exp $$
 */
public enum AibtonEnumErrorConfig implements IEnum<String,String,String> {

    /** JSON对象转换异常 **/
    SYSTEM_JACK_SON_ERROR(AibtonConstantKey.RESPONSE_000000,
            AibtonConstantKey.SYSTEM_JACK_SON_ERROR, AibtonConstantKey.SYSTEM),
    /** 用户没有权限访问该接口 **/
    USER_NOT_AUTH(AibtonConstantKey.RESPONSE_400000, AibtonConstantKey.USER_NOT_AUTH_ERROR,
            AibtonConstantKey.SYSTEM),
    /****/
    ;

    private String  code;

    private String  value;

    private String  group;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    AibtonEnumErrorConfig(String code, String value, String group) {
        this.code = code;
        this.value = value;
        this.group = group;
    }

    @Override
    public String getCode(String value, String group) {
        for (AibtonEnumErrorConfig e : AibtonEnumErrorConfig.values()) {
            if (e.getValue().equals(value) && e.getGroup().equals(group)) {
                return e.code;
            }
        }
        return null;
    }

    @Override
    public String getValue(String code, String group) {
        for (AibtonEnumErrorConfig e : AibtonEnumErrorConfig.values()) {
            if (e.getCode().equals(code) && e.getGroup().equals(group)) {
                return e.value;
            }
        }
        return null;
    }
}
