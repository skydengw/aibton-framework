/**
 * Aibton.com Inc.
 * Copyright (c) 2004-2017 All Rights Reserved.
 */
package com.aibton.framework.util;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;

import com.aibton.framework.enums.inter.IEnum;
import com.aibton.framework.exception.ExtItemException;

/**
 * 断言工具类
 * @author huzhihui
 * @version v 0.1 2017/5/9 22:10 huzhihui Exp $$
 */
public class AssertUtils {

    /**
     * 断言不为空；如果为null || ""  "  "    抛出异常终止执行
     * @param logger    Logger
     * @param value 待检查值
     * @param enumConfig    异常返回枚举
     */
    public static void isNotEmpty(Logger logger, String value, IEnum enumConfig) {
        if (null == value || value.trim().equals("")) {
            logger.error(String.valueOf(enumConfig.getValue()));
            throw new ExtItemException(enumConfig);
        }
    }

    /**
     * 断言不为空；如果为null || ""  "  "    抛出异常终止执行
     * @param logger    Logger
     * @param value 待检查值
     * @param msg   异常返回信息
     */
    public static void isNotEmpty(Logger logger, String value, String msg) {
        if (null == value || value.trim().equals("")) {
            logger.error(msg);
            throw new ExtItemException(msg);
        }
    }

    /**
     * 断言不为空；如果为null 抛出异常终止执行
     * @param logger    Logger
     * @param value 待检查值
     * @param enumConfig    异常返回枚举
     */
    public static void isNotNull(Logger logger, Object value, IEnum enumConfig) {
        if (null == value) {
            logger.error(String.valueOf(enumConfig.getValue()));
            throw new ExtItemException(enumConfig);
        }
    }

    /**
     * 断言不为空；如果为null 抛出异常终止执行
     * @param logger    Logger
     * @param value 待检查值
     * @param msg   异常返回信息
     */
    public static void isNotNull(Logger logger, Object value, String msg) {
        if (null == value) {
            logger.error(msg);
            throw new ExtItemException(msg);
        }
    }

    /**
     * 断言不为空；如果为null || list.size() == 0    抛出异常终止执行
     * @param logger    Logger
     * @param list  待检查值
     * @param enumConfig    异常返回枚举
     */
    public static void isNotEmpty(Logger logger, List list, IEnum enumConfig) {
        if (null == list || list.size() == 0) {
            logger.error(String.valueOf(enumConfig.getValue()));
            throw new ExtItemException(enumConfig);
        }
    }

    /**
     * 断言不为空；如果为null || list.size() == 0    抛出异常终止执行
     * @param logger    Logger
     * @param list  待检查值
     * @param msg   异常返回信息
     */
    public static void isNotEmpty(Logger logger, List list, String msg) {
        if (null == list || list.size() == 0) {
            logger.error(msg);
            throw new ExtItemException(msg);
        }
    }

    /**
     * 断言不为空；如果为null || map.size() == 0     抛出异常终止执行
     * @param logger    Logger
     * @param map   待检查值
     * @param enumConfig    异常返回枚举
     */
    public static void isNotEmpty(Logger logger, Map map, IEnum enumConfig) {
        if (null == map || map.size() == 0) {
            logger.error(String.valueOf(enumConfig.getValue()));
            throw new ExtItemException(enumConfig);
        }
    }

    /**
     * 断言不为空；如果为null || map.size() == 0     抛出异常终止执行
     * @param logger    Logger
     * @param map   待检查值
     * @param msg   异常返回信息
     */
    public static void isNotEmpty(Logger logger, Map map, String msg) {
        if (null == map || map.size() == 0) {
            logger.error(msg);
            throw new ExtItemException(msg);
        }
    }

    /**
     * 断言为空；如果为不为null 抛出异常终止执行
     * @param logger    Logger
     * @param value 待检查值
     * @param enumConfig    异常返回枚举
     */
    public static void isNull(Logger logger, Object value, IEnum enumConfig) {
        if (null != value) {
            logger.error(String.valueOf(enumConfig.getValue()));
            throw new ExtItemException(enumConfig);
        }
    }

    /**
     * 断言为true 如果为不为true 抛出异常终止执行
     * @param logger Logger
     * @param value 待检查值
     * @param msg 异常返回信息
     */
    public static void isTrue(Logger logger, Boolean value, String msg) {
        if (null == value || false == value) {
            logger.error(msg);
            throw new ExtItemException(msg);
        }
    }

    /**
     * 断言为true 如果为不为true 抛出异常终止执行
     * @param logger Logger
     * @param value 待检查值
     * @param enumConfig 异常返回枚举
     */
    public static void isTrue(Logger logger, Boolean value, IEnum enumConfig) {
        if (null == value || false == value) {
            logger.error(String.valueOf(enumConfig.getValue()));
            throw new ExtItemException(enumConfig);
        }
    }

    /**
     * 断言为true 如果为不为false 抛出异常终止执行
     * @param logger Logger
     * @param value 待检查值
     * @param msg 异常返回信息
     */
    public static void isFalse(Logger logger, Boolean value, String msg) {
        if (null == value || true == value) {
            logger.error(msg);
            throw new ExtItemException(msg);
        }
    }

    /**
     * 断言为true 如果为不为false 抛出异常终止执行
     * @param logger Logger
     * @param value 待检查值
     * @param enumConfig 异常返回枚举
     */
    public static void isFalse(Logger logger, Boolean value, IEnum enumConfig) {
        if (null == value || true == value) {
            logger.error((String) enumConfig.getValue());
            throw new ExtItemException(enumConfig);
        }
    }
}
